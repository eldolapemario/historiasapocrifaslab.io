---
layout: page
title: Hola...
permalink: /about/
---

…soy **eldolapemario**, te doy la bienvenida a mi espacio personal donde publicaré algunas de mis ficciones, relatos, crónicas y textos varios.

Podés compartirlas libremente. Si lo hacés, solo te pido que enlaces de manera directa al post correspondiente, y que menciones mi autoría. ¡Gracias!

Si me querés contactar, es aquí donde se puede: [**hacé clic**](mailto:eldolapemario@gmail.com)